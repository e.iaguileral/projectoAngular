'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">proyectofinal documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-bs-toggle="collapse" ${ isNormalMode ?
                                'data-bs-target="#modules-links"' : 'data-bs-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-AppModule-edb100bed8fadfa4430d8185add6ff8382607da0c17a787f6a3038a3314c217028f3a68eaa976f9c326cdb9df9045129b7e641583af79f1f5e22d9e9cec6a0c7"' : 'data-bs-target="#xs-components-links-module-AppModule-edb100bed8fadfa4430d8185add6ff8382607da0c17a787f6a3038a3314c217028f3a68eaa976f9c326cdb9df9045129b7e641583af79f1f5e22d9e9cec6a0c7"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-AppModule-edb100bed8fadfa4430d8185add6ff8382607da0c17a787f6a3038a3314c217028f3a68eaa976f9c326cdb9df9045129b7e641583af79f1f5e22d9e9cec6a0c7"' :
                                            'id="xs-components-links-module-AppModule-edb100bed8fadfa4430d8185add6ff8382607da0c17a787f6a3038a3314c217028f3a68eaa976f9c326cdb9df9045129b7e641583af79f1f5e22d9e9cec6a0c7"' }>
                                            <li class="link">
                                                <a href="components/AppComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AppComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                            </li>
                            <li class="link">
                                <a href="modules/AppRoutingModule.html" data-type="entity-link" >AppRoutingModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/ProjectModule.html" data-type="entity-link" >ProjectModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                            'data-bs-target="#components-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' : 'data-bs-target="#xs-components-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                            <span class="icon ion-md-cog"></span>
                                            <span>Components</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="components-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' :
                                            'id="xs-components-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                            <li class="link">
                                                <a href="components/GameComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >GameComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/HelpComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >HelpComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/LoginComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RankingComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RankingComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/RegisterComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >RegisterComponent</a>
                                            </li>
                                            <li class="link">
                                                <a href="components/StartpageComponent.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >StartpageComponent</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#directives-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' : 'data-bs-target="#xs-directives-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                        <span class="icon ion-md-code-working"></span>
                                        <span>Directives</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="directives-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' :
                                        'id="xs-directives-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                        <li class="link">
                                            <a href="directives/LoginvalidatorDirective.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LoginvalidatorDirective</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ?
                                        'data-bs-target="#injectables-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' : 'data-bs-target="#xs-injectables-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' :
                                        'id="xs-injectables-links-module-ProjectModule-26c4eff12bd7fbca5358b78eaba44a9d85af8f862a0515375bd22e5c6b9cee8f8a7598029f75c5bf4fa8c4a8e1c557ef06019fbdee43bee265fa6117916c306b"' }>
                                        <li class="link">
                                            <a href="injectables/UserformService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserformService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#classes-links"' :
                            'data-bs-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#injectables-links"' :
                                'data-bs-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/LoginService.html" data-type="entity-link" >LoginService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/UserformService.html" data-type="entity-link" >UserformService</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-bs-toggle="collapse" ${ isNormalMode ? 'data-bs-target="#miscellaneous-links"'
                            : 'data-bs-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <a data-type="chapter-link" href="routes.html"><span class="icon ion-ios-git-branch"></span>Routes</a>
                        </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank" rel="noopener noreferrer">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});