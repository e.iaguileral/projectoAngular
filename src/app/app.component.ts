import { Component } from '@angular/core';
import {LoginService} from "./shared/services/login.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent {
  title = 'proyectofinal';
  usuari!:any;
  constructor(private loginservice:LoginService) {
    this.loginservice.userlogin.subscribe((data: string) => this.usuari = data);
  }
  ngOnInit() {
    this.usuari = this.loginservice.userlogin.getValue();
  }
}
