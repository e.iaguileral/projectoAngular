import { Component } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {emailValidator, passwordValidator} from "../login/loginvalidator.directive";
import {UserformService} from "../../shared/services/userform.service";
import {User} from "../../shared/classes/user";
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  constructor (private connectbd:UserformService) { }
  regForm !: FormGroup;
  users : User[]=[];
  message! : string;
  ngOnInit(): void {
    this.regForm = new FormGroup( {
      nom : new FormControl('',
        [Validators.required,emailValidator(),Validators.minLength(7)]),
      password : new FormControl('',
        [Validators.required,passwordValidator(),Validators.minLength(8)])
    })
  }
  register() {
    this.connectbd.setUser((this.regForm))
      .subscribe(res => {
        console.log(res);
        this.message = res;
      });
  }
}
