import {Component, HostListener} from '@angular/core';
import {User} from "../../shared/classes/user";
import {LoginService} from "../../shared/services/login.service";
import {UserformService} from "../../shared/services/userform.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {emailValidator, passwordValidator} from "../login/loginvalidator.directive";
@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.css'
})

export class GameComponent {
  block:boolean = true;
  idInterval?:NodeJS.Timeout;
  spd:number = 300;
  capturades:number = 0;
  fallos:number = 0;
  gridarray:string[][] =[];
  logueado!:string;
  record!:number;
  usrForm!:FormGroup;
  finish:boolean = false;
  getRecord(){
    this.userform.getAllUsers().subscribe( res =>{
      for(let u of res){
        if(u.nom === this.logueado){
          this.record = u.punts;
        }
      }
    });
  }
  constructor(private loginservice:LoginService, private userform:UserformService) {
  }
  ngOnInit(){
    this.logueado = this.loginservice.userlogin.getValue();
    this.getRecord();

  }

  updateRecord() {
    this.userform.updateUser((this.usrForm))
      .subscribe(res => {
        console.log(res);
      });
  }
  comenzarPartida(){
    this.gridarray = [
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'eloi', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'dot-unhit', 'wall'],
      ['wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall', 'wall']
    ];
    this.spd = 300;
    this.generarActividad();
    clearInterval(this.idInterval);
    this.idInterval = setInterval(() => this.movimientoActividad(),this.spd);
    this.fallos = 0;
    this.capturades = 0;
    this.block = false;
    this.finish = false;

  }

  @HostListener('window:keydown', ['$event'])
  handleKeyDown(event: KeyboardEvent){
    if(!this.block){
      let f:number = 0;
      let c:number = 0;
      for(let i=0;i<this.gridarray.length;i++){
        for(let k=0;k<this.gridarray[0].length;k++){
          if(this.gridarray[i][k] === 'eloi'){
            f = i;
            c = k;
          }
        }
      }
      switch (event.code){
        case 'ArrowLeft':
          if(this.gridarray[f][c-1] === 'dot-unhit'){
            this.gridarray[f][c] = 'dot-unhit';
            this.gridarray[f][c-1] = 'eloi';
          }
          break;
        case 'ArrowRight':
          if(this.gridarray[f][c+1] === 'dot-unhit'){
            this.gridarray[f][c] = 'dot-unhit';
            this.gridarray[f][c+1] = 'eloi';
          }
          break;
      }
    }
  }

  generarActividad() {
    let generar:boolean = true;
    while(generar){
      const randomPos:number = Math.floor(Math.random() * (this.gridarray[0].length-1)+1);
      if(this.gridarray[1][randomPos] === 'dot-unhit'){
        this.gridarray[1][randomPos] = "act"
        generar = false;
      }
    }

  }

  movimientoActividad(){
    let f:number = 100;
    let c:number = 100;
    for(let i=0;i<this.gridarray.length;i++){
      for(let k=0;k<this.gridarray[0].length;k++){
        if(this.gridarray[i][k] === 'act'){
          f = i;
          c = k;
        }
      }
    }

    if(this.gridarray[f+1][c] === "dot-unhit"){
      this.gridarray[f+1][c] = "act";
      this.gridarray[f][c] = "dot-unhit";
    }
    else if(this.gridarray[f+1][c] === "wall"){
      this.gridarray[f][c] = "dot-unhit";
      this.fallos++;
      if(this.fallos >= 10){
        clearInterval(this.idInterval);
        this.finish = true;
        this.block = true;
        if(this.record < this.capturades){
          this.record = this.capturades;
          this.usrForm = new FormGroup( {
            nom : new FormControl(this.logueado),
            punts : new FormControl(this.record)
          })
          this.updateRecord();
          this.getRecord();
        }
      }else{
        this.generarActividad();
      }
    }
    else if(this.gridarray[f+1][c] === "eloi"){
      this.gridarray[f][c] = "dot-unhit";
      this.capturades++;
      this.generarActividad();
      if(this.capturades%20==0){
        this.spd -= 100;
        if(this.spd == 0){
          this.spd = 100;
        }
        clearInterval(this.idInterval);
        this.idInterval = setInterval(() => this.movimientoActividad(),this.spd);
      }
    }
  }
}
