import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StartpageComponent } from '../../../view/startpage/startpage.component';
import { GameComponent } from '../../../view/game/game.component';
import { LoginComponent } from '../../../view/login/login.component';
import { RegisterComponent } from '../../../view/register/register.component';
import { RankingComponent } from '../../../view/ranking/ranking.component';
import { HelpComponent } from '../../../view/help/help.component';
import {BrowserModule} from "@angular/platform-browser";
import {UserformService} from "../../services/userform.service";
import { LoginvalidatorDirective } from '../../../view/login/loginvalidator.directive';
import {FormsModule,ReactiveFormsModule} from "@angular/forms";
@NgModule({
  declarations: [
    StartpageComponent,
    GameComponent,
    LoginComponent,
    RegisterComponent,
    RankingComponent,
    HelpComponent,
    LoginvalidatorDirective
  ],
    imports: [
        CommonModule,
        BrowserModule,
        ReactiveFormsModule,
        FormsModule
    ],
  providers:[
    UserformService
  ],
  exports: [
    StartpageComponent,
    GameComponent,
    LoginComponent,
    RegisterComponent,
    RankingComponent,
    HelpComponent,
    LoginvalidatorDirective
  ]
})
export class ProjectModule { }
