export class User {
  id!:Number;
  nom!:string;
  password!:string;
  punts!:number;

  constructor(id: Number, nom: string, password: string, punts: number) {
    this.id = id;
    this.nom = nom;
    this.password = password;
    this.punts = punts;
  }
}
