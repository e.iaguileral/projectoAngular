import { TestBed } from '@angular/core/testing';

import { UserformService } from './userform.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {HttpClient} from "@angular/common/http";
import {FormControl, FormGroup} from "@angular/forms";

describe('UserformService', () => {
  let service: UserformService;
  let HttpClientSpy: jasmine.SpyObj<HttpClient>;
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    HttpClientSpy = jasmine.createSpyObj('HttpClient', ['get','post']);
    service = new UserformService(HttpClientSpy);
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers: [UserformService]
    });
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(UserformService);
  });

  it('no furros', () => {
    expect(service).toBeTruthy();

  });

  it('update url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      punts : new FormControl(10)
    })
    service.updateUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/update");
    expect(req.request.url).toBe(service.REST_API + "/user/update")
    expect(req.request.method).toEqual('POST');
  });

  it('update url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      punts : new FormControl(10)
    })
    service.updateUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/update");
    expect(req.request.url).toBe(service.REST_API + "/user/update")
    expect(req.request.method).toEqual('POST');
  });

  it('getusers url and metodo', () => {
    service.getAllUsers().subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/users");
    expect(req.request.url).toBe(service.REST_API + "/users")
    expect(req.request.method).toEqual('GET');
  });

  it('login url and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      password : new FormControl("Password123")
    })
    service.getLogin(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/users/login?nom=iker@ies-sabadell.cat&password=Password123");
    expect(req.request.url).toBe(service.REST_API + "/users/login")
    expect(req.request.method).toEqual('GET');
  });

  it('register url de furros gays amantes de las bicis y las gallinas and metodo', () => {
    let usrForm!:FormGroup;
    usrForm = new FormGroup( {
      nom : new FormControl("iker@ies-sabadell.cat"),
      password : new FormControl("Password123")
    })
    service.setUser(usrForm).subscribe({});
    const req = httpTestingController.expectOne(service.REST_API + "/user/insert");
    expect(req.request.url).toBe(service.REST_API + "/user/insert")
    expect(req.request.method).toEqual('POST');
  });
});
