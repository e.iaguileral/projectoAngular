import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {FormGroup} from "@angular/forms";

@Injectable({
  providedIn: 'root'
})
export class UserformService {
  REST_API: string = 'http://localhost:3000';

  constructor(private httpclient: HttpClient) {
  }

  public getAllUsers(): Observable<any> {
    return this.httpclient.get(`${this.REST_API}/users`);
  }

  public getLogin(form:FormGroup):Observable<any> {
    return this.httpclient.get(`${this.REST_API}/users/login`,
      {
        params: {
          nom: form.controls['nom'].value,
          password: form.controls['password'].value
        }
      });
  }
  public setUser(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/user/insert`, form.value);
  }

  public updateUser(form: FormGroup): Observable<any> {
    return this.httpclient.post(`${this.REST_API}/user/update`, form.value);
  }
}
